package com.example.demo.config;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;

@Configuration
public class JGitConfig {

    @Bean
    public Repository repository() throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.setGitDir(new File("path/to/your/repo/.git"))
                .readEnvironment()
                .findGitDir()
                .build();
        return repository;
    }
}