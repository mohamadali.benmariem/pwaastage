package com.example.demo.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import org.eclipse.jgit.lib.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BackendService {
    @Autowired
    private Repository repository; // Injectez le référentiel Git

    public void stageAndCommitLocally(Object data, String message) throws GitAPIException {
        Git git = new Git(repository);

        // Convertir les données en JSON et les sauvegarder localement
        String json = convertToJson(data);

        // Ajouter les modifications à l'index
        git.add().addFilepattern(".").call();

        // Faire un commit local
        git.commit().setMessage(message).call();
    }

    public void pushToRemote() throws GitAPIException {
        Git git = new Git(repository);

        // Pousser les changements vers le référentiel distant
        git.push().call();
    }

    private String convertToJson(Object data) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace(); // Gérer l'exception correctement dans une application réelle
            return "{}"; // Retourner un JSON vide en cas d'erreur
        }

        // Autres méthodes pour la gestion des branches, fusions, etc.
    }
}