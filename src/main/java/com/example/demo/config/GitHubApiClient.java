package com.example.demo.config;



import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
@Component
public class GitHubApiClient {

    private final String gitRepo = "https://api.github.com/repos/dalibm98/dalibmsss98"; // Replace with your repository
    private final String gitrowsToken = "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"; // Replace with your GitHub token
    private final RestTemplate restTemplate = new RestTemplate();

    public void createPullRequest() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Accept", "application/vnd.github.v3+json");
            headers.setBearerAuth(gitrowsToken);

            String branchName = "maain1"; // Replace with your branch name
            String baseBranch = "master"; // Replace with your base branch name

            // Prepare the request payload for creating a Pull Request
            String pullRequestJson = "{" +
                    "\"title\": \"My Pull Request\"," +
                    "\"head\": \"" + branchName + "\"," +
                    "\"base\": \"" + baseBranch + "\"," +
                    "\"body\": \"This is the description of the pull request.\"" +
                    "}";

            // Make the POST request to create a Pull Request
            HttpEntity<String> requestEntity = new HttpEntity<>(pullRequestJson, headers);
            ResponseEntity<String> response = restTemplate.exchange(
                    gitRepo , HttpMethod.POST, requestEntity, String.class);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                // Pull Request created successfully
                System.out.println("Pull Request created successfully.");
            } else {
                // Handle other response statuses
                System.err.println("Pull Request creation failed. Status: " + response.getStatusCode());
            }

        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
                // Handle validation errors
                String responseBody = e.getResponseBodyAsString();
                System.err.println("Validation Error: " + responseBody);
            } else {
                System.err.println("Error creating Pull Request: " + e.getMessage());
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            System.err.println("Error creating Pull Request: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
