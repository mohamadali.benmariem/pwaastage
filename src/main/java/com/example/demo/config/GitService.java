package com.example.demo.config;

import com.example.demo.model.Task;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.stereotype.Service;

@Service
public class GitService {
    public void pushClassDataToGit(String localRepositoryPath, String remoteRepositoryUrl, String username, String token, Task data) throws Exception {
        try (Repository repository = new FileRepository(localRepositoryPath + "/.git")) {
            Git git = new Git(repository);

            // Commit and push changes
            git.add().addFilepattern(".").call();
            git.commit().setMessage("Adding data from class").call();

            // Configure credentials for pushing
            CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(username, token);

            // Push changes
            git.push().setCredentialsProvider(credentialsProvider).setRemote(remoteRepositoryUrl).call();
        }
    }
}