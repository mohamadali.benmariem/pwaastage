package com.example.demo.contolluer;

import com.example.demo.config.BackendService;
import com.example.demo.model.DataWrapper;
import com.example.demo.model.Personne;
import com.example.demo.model.Task;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/con")
public class DataController {
    @Autowired
    private BackendService backendService;
    @Autowired
    private DataService dataService;

    @PostMapping("/update")
    public ResponseEntity<String> updateData(@RequestBody DataWrapper dataWrapper) {
        try {
            List<Task> tasks = dataWrapper.getTasks();
            List<Personne> personnes = dataWrapper.getPersonnes();

            dataService.updateData(tasks, personnes);

            return new ResponseEntity<>("Data updated successfully", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Error updating data", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/commit")
    public void commitLocally(@RequestBody Object data) throws GitAPIException {
        backendService.stageAndCommitLocally(data, "Auto-commit");
    }

    @PostMapping("/push")
    public void pushToRemote() throws GitAPIException {
        backendService.pushToRemote();
    }

}