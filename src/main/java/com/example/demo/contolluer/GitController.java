package com.example.demo.contolluer;

import com.example.demo.config.GitService;
import com.example.demo.model.Task;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/git")
public class GitController {

    private static final String GITROWS_API_URL = "https://api.gitrows.com/";
    private static final String OWNER = "@github";
    private static final String REPO = "dalibm98/dalibmsss98";
    private static final String FILE_PATH = "tasks/tasks.json";
    private static final String BEARER_TOKEN = "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";

    @PutMapping("/push")
    public ResponseEntity<String> pushDataToGit(@RequestBody Task data) {
        try {
            HttpClient httpClient = HttpClients.createDefault();
            HttpPut httpPost = new HttpPut(GITROWS_API_URL + OWNER + "/" + REPO + "/" + FILE_PATH);

            // Set headers
            httpPost.addHeader("Authorization", "Bearer " + BEARER_TOKEN);
            httpPost.addHeader("Content-Type", "application/json");

            // Convert Task data to JSON string
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonData = objectMapper.writeValueAsString(data);

            StringEntity entity = new StringEntity(jsonData);
            httpPost.setEntity(entity);

            // Execute the request
            HttpResponse response = httpClient.execute(httpPost);

            if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
                return ResponseEntity.ok("Data pushed successfully to GitRows.");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to push data to GitRows.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to push data to GitRows.");
        }
    }
}





