package com.example.demo.contolluer;

import com.example.demo.model.DataWrapper;
import com.example.demo.model.Personne;
import com.example.demo.model.Task;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataService {

    @Autowired
    private ObjectMapper objectMapper;
    private static final String OWNER = "dalibm98";
    private static final String REPO = "dalibmsss98";
    private static final String PATH = "tasks/data.json";

    @Autowired
    private RestTemplate restTemplate;

    public void updateData(List<Task> tasks, List<Personne> personnes) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(new DataWrapper(tasks, personnes));

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "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");
            headers.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<String> currentContentResponse = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, new HttpEntity<>(headers), String.class, OWNER, REPO, PATH);

            if (currentContentResponse.getStatusCode() == HttpStatus.OK) {
                String currentContent = currentContentResponse.getBody();
                JSONObject currentJson = new JSONObject(currentContent);
                String currentSha = currentJson.getString("sha");

                // Créez votre objet payload avec le contenu JSON généré
                JSONObject payload = new JSONObject();
                payload.put("message", "Updating data");
                payload.put("content", Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8)));
                payload.put("sha", currentSha);

                HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

                ResponseEntity<String> response = restTemplate.exchange(
                        "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                        HttpMethod.PUT, request, String.class, OWNER, REPO, PATH);

                if (response.getStatusCode() == HttpStatus.CREATED) {
                    System.out.println("Data updated successfully");
                }
            } else {
                System.out.println("Error getting current content: " + currentContentResponse.getStatusCode());
            }

        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
        }
    }
}

