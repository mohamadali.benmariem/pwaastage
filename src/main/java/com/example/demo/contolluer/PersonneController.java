package com.example.demo.contolluer;

import com.example.demo.model.DataWrapper;
import com.example.demo.model.Personne;
import com.example.demo.model.Task;
import com.example.demo.service.PersonneService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;

@RestController
@RequestMapping("/personnes")
public class PersonneController {

    @Autowired
    private PersonneService personneService;
/*
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Personne createPersonne(@RequestBody Personne personne) {
        // Appel au service pour création
        personneService.createPersonne(personne);
        return personne;
    }

 */

    @GetMapping("/single")
    public ResponseEntity<List<Personne>> getPersonne() {
        List<Personne> personnes = personneService.getPersonnes();
        return new ResponseEntity<>(personnes, HttpStatus.OK);
    }

    @GetMapping("/batch")
    public ResponseEntity<List<Personne>> getPersonnes() {
        List<Personne> personnes = personneService.getPersonnes();
        return new ResponseEntity<>(personnes, HttpStatus.OK);
    }

    @PutMapping("/batch")
    public ResponseEntity<String> createPersonnesBatch(@RequestBody Personne personne) {
        try {
            personneService.createpersonne(personne);
            return new ResponseEntity<>("Personnes created successfully", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Error creating personnes", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
