package com.example.demo.contolluer;

import com.example.demo.config.GitHubApiClient;
import com.example.demo.config.GithubFile;
import com.example.demo.model.Employee;
import com.example.demo.model.Task;
import com.example.demo.service.EmployeeService;
import com.example.demo.service.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService taskService;
    private final EmployeeService employeeService;

    @Autowired
    public TaskController(TaskService taskService, EmployeeService employeeService) {
        this.taskService = taskService;
        this.employeeService = employeeService;
    }


    @GetMapping
    public ResponseEntity<List<Task>> getTasks() {
        List<Task> tasks = taskService.getTasks();
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @PostMapping
    public void createTask(@RequestBody Task task, @RequestParam String assigneeId) {
        Employee assignee = employeeService.getEmployeeById(assigneeId);
        taskService.createTask(task, assignee);
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<String> updateTask(@PathVariable String taskId, @RequestBody Task task) {
        task.setId(taskId);
        taskService.updateTask(task);
        return ResponseEntity.ok("Task updated successfully");
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<Task> getTaskById(@PathVariable String taskId) {
        Task task = taskService.getTaskById(taskId);

        if (task != null) {
            return ResponseEntity.ok(task);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{taskId}")
    public ResponseEntity<String> deleteTask(@PathVariable String taskId) {
        taskService.deleteTask(taskId);
        return ResponseEntity.ok("Task deleted successfully");
    }
    @GetMapping("/employees/{employeeId}/tasks")
    public List<Task> getTasksByEmployee(@PathVariable String employeeId) {
        Employee employee = employeeService.getEmployeeById(employeeId);
        return taskService.getTasksByEmployee(employee);
    }


}
