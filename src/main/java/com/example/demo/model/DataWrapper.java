package com.example.demo.model;

import java.util.List;

public class DataWrapper {
    private List<Task> tasks;
    private List<Personne> personnes;


    public DataWrapper(List<Task> tasks, List<Personne> personnes) {
        this.tasks = tasks;
        this.personnes = personnes;

    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(List<Personne> personnes) {
        this.personnes = personnes;
    }
}
