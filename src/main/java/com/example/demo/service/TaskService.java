package com.example.demo.service;

import com.example.demo.model.Employee;
import com.example.demo.model.Task;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FileUtils;
import org.bson.json.JsonObject;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import java.io.*;
import java.net.*;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.eclipse.jgit.lib.Repository;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TaskService {
  //  private final String repoPath = "C:/Users/Dali/OneDrive/Bureau/demo1" ;
  //  private final String apiUrl = "https://api.gitrows.com/@github/dalibm98/pws/tasks/tasks.json";

  //  private final String remoteRepoURI = "https://github.com/dalibm98/dalibmsss98.git";

 //   private static final String OWNER = "dalibm98";
  //  private static final String REPO = "dalibmsss98";
   // private static final String PATH = "tasks/task.json";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EmployeeService employeeService;

   // @Value("${github.token}")
  //  private String githubToken;


    public List<Task> getTasks() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("PRIVATE-TOKEN", "glpat-j4G8apMcqRtYibU-uP1Z");
            HttpEntity<Void> request = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://gitlab.com/api/v4/projects/48721917/repository/files/task.json?ref=main",
                    HttpMethod.GET, request, String.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error: " + response.getStatusCode());
            }

            String content = response.getBody();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseNode = mapper.readTree(content);

            if (responseNode.has("content")) {
                String encodedContent = responseNode.get("content").asText();
                byte[] decodedBytes = Base64.getDecoder().decode(encodedContent);
                String decodedContent = new String(decodedBytes, StandardCharsets.UTF_8);
                return mapper.readValue(decodedContent, new TypeReference<List<Task>>() {});
            } else {
                throw new IOException("Response does not contain 'content' field.");
            }
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    private void saveTasks(List<Task> tasks) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(tasks);

            HttpHeaders headers = new HttpHeaders();
            headers.set("PRIVATE-TOKEN", "glpat-j4G8apMcqRtYibU-uP1Z");
            headers.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<String> currentContentResponse = restTemplate.exchange(
                    "https://gitlab.com/api/v4/projects/48721917/repository/files/task.json?ref=main",
                    HttpMethod.GET, new HttpEntity<>(headers), String.class);

            if (currentContentResponse.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error getting current content: " + currentContentResponse.getStatusCode());
            }

            String currentContent = currentContentResponse.getBody();
            JsonNode currentJson = mapper.readTree(currentContent);
            String currentSha = currentJson.get("commit_id").asText();

            JsonNode payload = mapper.createObjectNode();
            ((ObjectNode) payload).put("branch", "main");
            ((ObjectNode) payload).put("commit_message", "Updating tasks");
            ((ObjectNode) payload).put("content", Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8)));
            ((ObjectNode) payload).put("encoding", "base64");
            ((ObjectNode) payload).put("sha", currentSha);

            HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://gitlab.com/api/v4/projects/48721917/repository/files/task.json?ref=main",
                    HttpMethod.PUT, request, String.class);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                System.out.println("Tasks saved successfully");
            }
        } catch (IOException e) {
            // Handle exceptions according to your application's needs
        }
    }

    public void createTask(Task task, Employee assignee) {
        if (assignee != null) {
            UUID uniqueId = UUID.randomUUID();
            task.setId(uniqueId.toString());
            task.setAssignee(assignee);

            if (assignee.getTasks() == null) {
                assignee.setTasks(new ArrayList<>());
            }
            assignee.getTasks().add(task);
            employeeService.updateEmployee(assignee);
        }

        List<Task> tasks = getTasks();
        tasks.add(task);
        saveTasks(tasks);
    }

    public void updateTask(Task updatedTask) {
        try {
            List<Task> tasks = getTasks();
            Optional<Task> taskToUpdate = tasks.stream()
                    .filter(task -> task.getId().equals(updatedTask.getId()))
                    .findFirst();

            if (taskToUpdate.isPresent()) {
                Task existingTask = taskToUpdate.get();
                existingTask.setStatus(updatedTask.getStatus());
                existingTask.setDescription(updatedTask.getDescription());

                saveTasks(tasks);
            } else {
                System.out.println("Task updated Not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTask(String taskId) {
        try {
            List<Task> tasks = getTasks();
            tasks.removeIf(task -> task.getId().equals(taskId));
            saveTasks(tasks);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Task> getTasksByEmployee(Employee employee) {
        List<Task> allTasks = getTasks();
        List<Task> employeeTasks = new ArrayList<>();

        for (Task task : allTasks) {
            if (task != null && task.getAssignee() != null && task.getAssignee().getId().equals(employee.getId())) {
                employeeTasks.add(task);
            }
        }

        return employeeTasks;
    }

    public Task getTaskById(String taskId) {
        List<Task> tasks = getTasks();
        for (Task task : tasks) {
            if (task != null && task.getId().equals(taskId)) {
                return task;
            }
        }
        return null;
    }








/*
    public List<Task> getTasks() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + githubToken);
            HttpEntity<Void> request = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, request, String.class, OWNER, REPO, PATH);

            if (response.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error: " + response.getStatusCode());
            }

            String content = response.getBody();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseNode = mapper.readTree(content);

            if (responseNode.has("content")) {
                String encodedContent = responseNode.get("content").asText();
                byte[] decodedBytes = Base64.getMimeDecoder().decode(encodedContent);
                String decodedContent = new String(decodedBytes, StandardCharsets.UTF_8);
                return mapper.readValue(decodedContent, new TypeReference<List<Task>>() {});
            } else {
                throw new IOException("Response does not contain 'content' field.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public void createTask(Task task, Employee assignee) {
        System.out.println("Assignee: " + assignee);

        if (assignee != null) {
            UUID uniqueId = UUID.randomUUID();
            task.setId(uniqueId.toString());
            task.setAssignee(assignee);

            // Ajouter la tâche à la liste de tâches de l'employé
            if (assignee.getTasks() == null) {
                assignee.setTasks(new ArrayList<>());
            }
            assignee.getTasks().add(task);

            // Mettre à jour l'employé dans la liste des employés
            employeeService.updateEmployee(assignee);
        }

        List<Task> tasks = getTasks();
        tasks.add(task);
        saveTasks(tasks);
        System.out.println("Task created successfully");
    }



    public void updateTask(Task updatedTask) {
        try {
            List<Task> tasks = getTasks();
            Optional<Task> taskToUpdate = tasks.stream()
                    .filter(task -> task.getId().equals(updatedTask.getId()))
                    .findFirst();

            if (taskToUpdate.isPresent()) {
                Task existingTask = taskToUpdate.get();
                existingTask.setStatus(updatedTask.getStatus());
                existingTask.setDescription(updatedTask.getDescription());
                // Update other properties if needed

                saveTasks(tasks);
                System.out.println("Task updated successfully");
            } else {
                // Handle task not found
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTask(String taskId) {
        try {
            List<Task> tasks = getTasks();
            tasks.removeIf(task -> task.getId().equals(taskId));
            saveTasks(tasks);
            System.out.println("Task deleted successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void saveTasks(List<Task> tasks) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(tasks);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + githubToken);
            headers.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<String> currentContentResponse = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, new HttpEntity<>(headers), String.class, OWNER, REPO, PATH);

            if (currentContentResponse.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error getting current content: " + currentContentResponse.getStatusCode());
            }

            String currentContent = currentContentResponse.getBody();
            org.json.JSONObject currentJson = new org.json.JSONObject(currentContent);
            String currentSha = currentJson.getString("sha");

            org.json.JSONObject payload = new org.json.JSONObject();
            payload.put("message", "Updating tasks");
            payload.put("content", Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8)));
            payload.put("sha", currentSha);

            HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.PUT, request, String.class, OWNER, REPO, PATH);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                System.out.println("Tasks saved successfully");
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Gérer l'exception selon les besoins de votre application
        }
    }



    public List<Task> getTasksByEmployee(Employee employee) {
        List<Task> allTasks = getTasks();
        List<Task> employeeTasks = new ArrayList<>();

        for (Task task : allTasks) {
            if (task != null && task.getAssignee() != null) {
                System.out.println("Task Assignee ID: " + task.getAssignee().getId());
                System.out.println("Employee ID: " + employee.getId());
                if (task.getAssignee().getId().equals(employee.getId())) {
                    employeeTasks.add(task);
                }
            }
        }

        return employeeTasks;
    }


    public Task getTaskById(String taskId) {
        List<Task> tasks = getTasks();
        for (Task task : tasks) {
            if (task != null && task.getId().equals(taskId)) {
                return task;
            }
        }
        return null; // Task not found
    }

}


/*

    public void addTask(Task task) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            // Lire le contenu existant du fichier
            File taskFile = new File(repoPath + "/tasks/tasks.json");
            List<Task> existingTasks = new ArrayList<>();
            if (taskFile.exists()) {
                String existingTasksJson = FileUtils.readFileToString(taskFile, StandardCharsets.UTF_8);
                existingTasks = mapper.readValue(existingTasksJson, new TypeReference<List<Task>>() {});
            }

            // Ajouter la nouvelle tâche à la liste existante
            existingTasks.add(task);

            // Écrire la liste mise à jour dans le fichier
            String updatedTasksJson = mapper.writeValueAsString(existingTasks);
            FileUtils.writeStringToFile(taskFile, updatedTasksJson, StandardCharsets.UTF_8);

            // Ouvrir le référentiel Git
            try (Repository repository = new FileRepositoryBuilder().setGitDir(new File(repoPath + "/.git")).build()) {
                // Créer une instance Git à partir du référentiel
                try (Git git = new Git(repository)) {
                    git.add().addFilepattern("tasks/tasks.json").call();
                    git.commit().setMessage("Added task: " + task.getId()).call();

                    // Pousser les modifications vers le référentiel Git distant en utilisant le token d'accès personnel
                    git.push()
                            .setRemote(remoteRepoURI)
                            .setCredentialsProvider(new UsernamePasswordCredentialsProvider("token", personalAccessToken))
                            .setRefSpecs(new RefSpec("refs/heads/master:refs/heads/master")) // Mettez la branche appropriée ici
                            .call();
                }
            } catch (GitAPIException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public Task getTask(String taskId) {
        try {
            // Perform GET request to API URL
            URL url = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Read response from the API
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                // Parse the JSON response
                ObjectMapper mapper = new ObjectMapper();
                JsonNode tasksNode = mapper.readTree(response.toString());

                // Find the task with the specified taskId
                for (JsonNode taskNode : tasksNode) {
                    String id = taskNode.get("id").asText();
                    if (id.equals(taskId)) {
                        // Convert JSON to Task object
                        return mapper.treeToValue(taskNode, Task.class);
                    }
                }
            } else {
                // Handle error
                System.out.println("Failed to retrieve tasks: HTTP " + responseCode);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<RevCommit> getGitHistory() {
        try (Repository repository = new FileRepositoryBuilder().setGitDir(new File(repoPath + "/.git")).build()) {
            try (Git git = new Git(repository)) {
                Iterable<RevCommit> commits = git.log().all().call();
                return StreamSupport.stream(commits.spliterator(), false)
                        .collect(Collectors.toList());
            }
        } catch (IOException | GitAPIException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }

 */
    }




