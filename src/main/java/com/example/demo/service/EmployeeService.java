package com.example.demo.service;

import com.example.demo.model.Employee;

import com.example.demo.model.Task;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service

public class EmployeeService {
    private static final String EMPLOYEE_PATH = "tasks/gir.json";
    private static final String OWNER = "dalibm98";
    private static final String REPO = "dalibmsss98";
    @Autowired
    private RestTemplate restTemplate;

    @Value("${github.token}")
    private String githubToken;

    public List<Employee> getEmployees() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + githubToken);
            HttpEntity<Void> request = new HttpEntity<>(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, request, String.class, OWNER, REPO, EMPLOYEE_PATH);

            if (response.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error: " + response.getStatusCode());
            }

            String content = response.getBody();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseNode = mapper.readTree(content);

            if (responseNode.has("content")) {
                String encodedContent = responseNode.get("content").asText();
                byte[] decodedBytes = Base64.getMimeDecoder().decode(encodedContent);
                String decodedContent = new String(decodedBytes, StandardCharsets.UTF_8);
                return mapper.readValue(decodedContent, new TypeReference<List<Employee>>() {});
            } else {
                throw new IOException("Response does not contain 'content' field.");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public void createEmployee(Employee employee) {
        List<Employee> employees = getEmployees();
        UUID uniqueId = UUID.randomUUID();
        employee.setId(uniqueId.toString());
        employees.add(employee);

        saveEmployees(employees);

        System.out.println("Employee created successfully");
    }

    // Vous pouvez ajouter des méthodes pour mettre à jour et supprimer un employé ici

    private void saveEmployees(List<Employee> employees) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(employees);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + githubToken);
            headers.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<String> currentContentResponse = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, new HttpEntity<>(headers), String.class, OWNER, REPO, EMPLOYEE_PATH);

            if (currentContentResponse.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error getting current content: " + currentContentResponse.getStatusCode());
            }

            String currentContent = currentContentResponse.getBody();
            org.json.JSONObject currentJson = new org.json.JSONObject(currentContent);
            String currentSha = currentJson.getString("sha");

            org.json.JSONObject payload = new org.json.JSONObject();
            payload.put("message", "Updating employees");
            payload.put("content", Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8)));
            payload.put("sha", currentSha);

            HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.PUT, request, String.class, OWNER, REPO, EMPLOYEE_PATH);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                System.out.println("Employees saved successfully");
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Gérer l'exception selon les besoins de votre application
        }
    }
    public void updateEmployee(Employee employee) {
        List<Employee> employees = getEmployees();
        Optional<Employee> employeeToUpdate = employees.stream()
                .filter(emp -> emp.getId().equals(employee.getId()))
                .findFirst();

        if (employeeToUpdate.isPresent()) {
            employees.remove(employeeToUpdate.get());
            employees.add(employee);
            saveEmployees(employees);
            System.out.println("Employee updated successfully");
        } else {
            System.out.println("Employee not found");
        }
    }


    public Employee getEmployeeById(String employeeId) {
        List<Employee> employees = getEmployees();
        Employee foundEmployee = employees.stream()
                .filter(employee -> employee.getId().equals(employeeId))
                .findFirst()
                .orElse(null);

        if (foundEmployee != null) {
            System.out.println("Found employee with ID " + employeeId + ": " + foundEmployee.getName());
        } else {
            System.out.println("No employee found with ID: " + employeeId);
        }

        return foundEmployee;
    }


}
