package com.example.demo.service;

import com.example.demo.model.DataWrapper;
import com.example.demo.model.Personne;

import com.example.demo.model.Task;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.List;


@Service
public class PersonneService {
    private static final Logger LOG = LoggerFactory.getLogger(PersonneService.class);

    @Autowired
    private ObjectMapper objectMapper;


    private final String gitRepo = "https://api.github.com/repos/dalibm98/pws";
    String gitrowsToken = "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";



    private static final String OWNER = "dalibm98";
    private static final String REPO = "dalibmsss98";
    private static final String PATH = "tasks/data.json";

    @Autowired
    private RestTemplate restTemplate;



    public List<Personne> getPersonnes() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer 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");
            HttpEntity request = new HttpEntity(headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, request, String.class, OWNER, REPO, PATH);

            if (response.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error: " + response.getStatusCode());
            }

            String content = response.getBody();

            // Parse the JSON response directly
            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseNode = mapper.readTree(content);

            if (responseNode.has("content")) {
                String encodedContent = responseNode.get("content").asText();

                // Decode the Base64 content
                byte[] decodedBytes = Base64.getMimeDecoder().decode(encodedContent);
                String decodedContent = new String(decodedBytes, StandardCharsets.UTF_8);

                List<Personne> personnes = mapper.readValue(decodedContent, new TypeReference<List<Personne>>() {});
                return personnes;
            } else {
                throw new IOException("Response does not contain 'content' field.");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }



    public void createpersonne(Personne per) throws JsonProcessingException {
        try {
            List<Personne> personnes = getPersonnes();
            personnes.add(per);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(personnes);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer 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");
            headers.setContentType(MediaType.APPLICATION_JSON);

            // Get the current content and SHA of the file
            ResponseEntity<String> currentContentResponse = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.GET, new HttpEntity<>(headers), String.class, OWNER, REPO, PATH);

            if (currentContentResponse.getStatusCode() != HttpStatus.OK) {
                throw new IOException("Error getting current content: " + currentContentResponse.getStatusCode());
            }

            String currentContent = currentContentResponse.getBody();
            org.json.JSONObject currentJson = new org.json.JSONObject(currentContent);
            String currentSha = currentJson.getString("sha");

            // Create a request payload with the updated content and SHA
            org.json.JSONObject payload = new org.json.JSONObject();
            payload.put("message", "Adding a new personnes");
            payload.put("content", Base64.getEncoder().encodeToString(json.getBytes(StandardCharsets.UTF_8)));
            payload.put("sha", currentSha); // Include the current SHA

            HttpEntity<String> request = new HttpEntity<>(payload.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(
                    "https://api.github.com/repos/{owner}/{repo}/contents/{path}",
                    HttpMethod.PUT, request, String.class, OWNER, REPO, PATH);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                System.out.println("Personnes created successfully");
            }

        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
        }
    }




/*

    public void createPersonne(Personne personne) {
        try {
            String personneJson = objectMapper.writeValueAsString(personne);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(gitrowsToken);

            // Create a request entity with headers and JSON payload
            HttpEntity<String> requestEntity = new HttpEntity<>(personneJson, headers);

            // Construct the API endpoint for creating content
            String url = gitRepo + "/contents/file.json"; // Update the file path

            // Make the POST request
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);

            if (response.getStatusCode() == HttpStatus.CREATED) {
                // Content creation successful
                System.out.println("Content created successfully.");
            } else {
                // Handle other response statuses
                System.err.println("Content creation failed. Status: " + response.getStatusCode());
            }

        } catch (JsonProcessingException e) {
            System.err.println("Error serializing Personne: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Personne getPersonne(Long id) {
        try {
            String personneJson = restTemplate.getForObject(gitRepo + "/contents/", String.class);
            return objectMapper.readValue(personneJson, Personne.class);

        } catch (JsonProcessingException e) {
            LOG.error("Erreur désérialisation Personne: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /*
    public void updatePersonne(Personne personne) {
        // Sérialisation JSON
        String personneJson = new ObjectMapper().writeValueAsString(personne);

      String url = gitRepo + "/contents/" + id;
restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
    }

     */

    public void deletePersonne(Long id) {
        // Requête DELETE pour supprimer le fichier
        restTemplate.delete(gitRepo + "/contents/personnes/" + id + ".json");
    }

}